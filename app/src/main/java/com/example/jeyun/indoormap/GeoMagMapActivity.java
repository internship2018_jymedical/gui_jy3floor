package com.example.jeyun.indoormap;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;

import java.util.ArrayList;
import java.util.Arrays;

public class GeoMagMapActivity extends AppCompatActivity {

    PhotoView photoview;
    Paint paint;
    Canvas canvas;
    Bitmap originBitMap, resultBitMap;
    int MAX_PIXEL_WIDTH = 540;   //10pixel에 1칸
    int MAX_PIXEL_HEIGHT = 1060;
    int pos_ZeroX = 30;
    int pos_ZeroY = 98;
    float currentDirection = 0;
    int bitmapSizeX;
    int bitmapSizeY;

    float maxX, maxY, maxZ, minX, minY, minZ;
    float map_X, map_Y, map_Z;
    float[] pos_arr = new float[3];

    // db 배정.
    MySQLiteOpenHelper mySQLiteOpenHelper_third;
    DBHelper dbHelper_third;
    SQLiteDatabase third_mDB;
    String dbName;
    Cursor cursor;

    //Spinner
    Spinner spinner_angle;
    ArrayList<String> angle_arr = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_mag_map);
        //뒤로가기 버튼 생성
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //실행시 방향 받아오기
        Intent intent1 = getIntent();
        currentDirection = intent1.getFloatExtra("DIRECTION", 0);
        photoview = (PhotoView) findViewById(R.id.iv_geoMagMap_GeoMagAct);
        photoview.setImageResource(R.drawable.floor3_excel_final);
        //mAttacher.setScaleType(ImageView.ScaleType.FIT_XY); //화면에 꽉 차게

        //Image init
        originBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.floor3_excel_final);
        bitmapSizeX = originBitMap.getWidth(); bitmapSizeY = originBitMap.getHeight();
        resultBitMap = Bitmap.createBitmap(bitmapSizeX, bitmapSizeY, Bitmap.Config.RGB_565);
        System.out.println("Direction : " + currentDirection + ", " + "bitmap_size : " + bitmapSizeX + ", " + bitmapSizeY);

        // DB 이름 배정
        dbName = "new_third.db";
        dbName = intent1.getStringExtra("DBNAME");
        //DB 값 복사 후 앱에 저장 //새로운 지도 = 새로운 helper ==> 추후 수정
        mySQLiteOpenHelper_third = new MySQLiteOpenHelper(getApplicationContext(), dbName);
        //DB 사용 등록
        dbHelper_third = new DBHelper(getApplicationContext(), dbName, null, 1);
        //sql db 배정

        drawMapFromDB();

        third_mDB = dbHelper_third.getReadableDatabase();
        cursor = third_mDB.rawQuery("SELECT DISTINCT pos_Theta FROM data", null);
        while (cursor.moveToNext()) {
            angle_arr.add(cursor.getString(0));
        }
        third_mDB.close();
        spinner_angle = (Spinner) findViewById(R.id.spinner_Angle_GeoMagAct);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                angle_arr.toArray(new String[angle_arr.size()])
        );
        spinner_angle.setAdapter(adapter);
        for ( int i=0; i < angle_arr.size(); i++ ) {
            if ( Float.parseFloat(angle_arr.get(i)) == currentDirection ) {
                spinner_angle.setSelection(i);
            }
        }
        spinner_angle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println(Float.parseFloat(angle_arr.get(i)));
                currentDirection = Float.parseFloat(angle_arr.get(i));
                drawMapFromDB();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setDBName(String db) { dbName = db; }

    public void drawMapFromDB() {

        third_mDB = dbHelper_third.getReadableDatabase();

        //각 field의 min,max 값을 이용하여 상대적인 색으로 표현한다. 그러기 위해서 색의 범위를 찾는 부분
        cursor = third_mDB.rawQuery("SELECT MAX(mag_X) FROM data", null);
        while (cursor.moveToNext()) {
            maxX = cursor.getFloat(0);
            System.out.println("mag_X_max : " + maxX);
        }
        cursor = third_mDB.rawQuery("SELECT MIN(mag_X) FROM data", null);
        while (cursor.moveToNext()) {
            minX = cursor.getFloat(0);
            System.out.println("mag_X_min : " + minX);
        }
        cursor = third_mDB.rawQuery("SELECT MAX(mag_Y) FROM data", null);
        while (cursor.moveToNext()) {
            maxY = cursor.getFloat(0);
            System.out.println("mag_Y_max : " + maxY);
        }
        cursor = third_mDB.rawQuery("SELECT MIN(mag_Y) FROM data", null);
        while (cursor.moveToNext()) {
            minY = cursor.getFloat(0);
            System.out.println("mag_Y_minY : " + minY);
        }
        cursor = third_mDB.rawQuery("SELECT MAX(mag_Z) FROM data", null);
        while (cursor.moveToNext()) {
            maxZ = cursor.getFloat(0);
            System.out.println("mag_Z_max : " + maxZ);
        }
        cursor = third_mDB.rawQuery("SELECT MIN(mag_Z) FROM data", null);
        while (cursor.moveToNext()) {
            minZ = cursor.getFloat(0);
            System.out.println("mag_Z_max : " + minZ);
        }

        cursor = third_mDB.rawQuery("SELECT * FROM data WHERE pos_theta==" + currentDirection, null);
        resultBitMap.recycle();
        resultBitMap = Bitmap.createBitmap(bitmapSizeX, bitmapSizeY, Bitmap.Config.RGB_565);
        canvas = new Canvas(resultBitMap);
        canvas.drawBitmap(originBitMap, 0, 0, null);
        while (cursor.moveToNext()) {
            map_X = cursor.getFloat(1);
            map_Y = cursor.getFloat(2);
            map_Z = cursor.getFloat(3);
            pos_arr[0] = cursor.getInt(4);   // pos_X
            pos_arr[1] = cursor.getInt(5);   // pos_Y
            pos_arr[2] = cursor.getFloat(6);        // theta

            //System.out.println(map_X + ", " + map_Y + ", " + map_Z + ",\n" + pos_arr[0] + ", " + pos_arr[1] + ", " + pos_arr[2]);
            paint = new Paint();
            paint.setColor(Color.rgb( (int) ((map_X-minX)*255/(maxX-minX)), (int) ((map_Y-minY)*255/(maxY-minY)), (int) ((map_Z-minZ)*255/(maxZ-minZ)) ));
            canvas.drawRect((pos_ZeroX+(pos_arr[0]*2))*((float) bitmapSizeX/MAX_PIXEL_WIDTH*10), (pos_ZeroY-(pos_arr[1]*2)+1)*((float)bitmapSizeY/MAX_PIXEL_HEIGHT*10), (pos_ZeroX+(pos_arr[0]*2)+1)*((float) bitmapSizeX/MAX_PIXEL_WIDTH*10), (pos_ZeroY-(pos_arr[1]*2))*((float)bitmapSizeY/MAX_PIXEL_HEIGHT*10), paint);
        }
        photoview.setImageBitmap(resultBitMap);

        third_mDB.close();

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        //GeoMap 버튼 클릭 시
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
