package com.example.jeyun.indoormap;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by gksdu on 2018-01-12.
 */

//내부로 옮겨진 DB를 사용하기 위한 Class
public class DBHelper extends SQLiteOpenHelper {

    SQLiteDatabase mDB;
    int[] raw_pos, unit_pos, norm_pos;
    int test = 0;

    // DBHelper 생성자로 관리할 DB 이름과 버전 정보를 받음
    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        raw_pos = new int[3];
        unit_pos = new int[3];
        norm_pos = new int[3];

        String path =  "/data/data/" + context.getApplicationContext().getPackageName() + "/databases/" + name;

        mDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
    }

    // DB를 새로 생성할 때 호출되는 함수
    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    // DB 업그레이드를 위해 버전이 변경될 때 호출되는 함수
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public SQLiteDatabase getmDB()
    {
        return mDB;
    }

    public void insert(String create_at, String item, int price) {
        // 읽고 쓰기가 가능하게 DB 열기
        SQLiteDatabase db = getWritableDatabase();
        // 추가
        db.close();
    }

    public void update(String item, int price) {
        SQLiteDatabase db = getWritableDatabase();
        // 수정
        db.close();
    }

    public void delete(String item) {
        SQLiteDatabase db = getWritableDatabase();
        // 삭제
        db.close();
    }

    public int[] getResult(double mag_X, double mag_Y, double mag_Z) {
        // 읽기가 가능하게 DB 열기
        //Log.i("dbpath", mDB.getPath());
        double minRMS = 0, curRMS = 0;
        double map_X = 0, map_Y = 0, map_Z = 0;                         // 지도상의 magnetic x,y,z;
        double normRMS_min = 0, unitRMS_min = 0, normRMS_cur = 0, unitRMS_cur = 0;
        double m_v_leng = 0, c_v_leng = 0;
        double[] c_unit, m_unit, c_norm, m_norm;
        String result = "";
        int pos_X, pos_Y, pos_Theta;

        //unit벡터 공간과  norm 벡터 공간을 위한 변수
        c_unit = new double[3];
        m_unit = new double[3];
        c_norm = new double[3];
        m_norm = new double[3];

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용
        Cursor cursor = mDB.rawQuery("SELECT * FROM data", null);
        int check = 0;
        test = 0;

        //단위벡터공간 비교를 위한 벡터 길이 구하기(현재 지자기)
        c_v_leng = Math.sqrt(mag_X * mag_X + mag_Y * mag_Y + mag_Z * mag_Z);
        c_unit[0] = mag_X / c_v_leng;
        c_unit[1] = mag_Y / c_v_leng;
        c_unit[2] = mag_Z / c_v_leng;

        // Map 값과 현재 위치의 자기장을 비교하는 알고리즘 부분이면서 DB의 값과 비교하는 부분이기도 함.
        //변화하지 않는 값 : mag , cursor에 따라 변화하는 값 : map
        while (cursor.moveToNext()) {

            // cursor에서 데이터 receive
            map_X = cursor.getDouble(1);
            map_Y = cursor.getDouble(2);
            map_Z = cursor.getDouble(3);
            pos_X = cursor.getInt(4);
            pos_Y = cursor.getInt(5);
            pos_Theta = cursor.getInt(6);

            if (check == 0) {
                // 처음 초기화 부분
                raw_pos[0] = pos_X;   // pos_X
                raw_pos[1] = pos_Y;   // pos_Y
                raw_pos[2] = pos_Theta;        // theta

                unit_pos[0] = pos_X;
                unit_pos[1] = pos_Y;
                unit_pos[2] = pos_Theta;

                norm_pos[0] = pos_X;
                norm_pos[1] = pos_Y;
                norm_pos[2] = pos_Theta;

                // RAW DATA RMS 알고리즘 이용.
                minRMS = (map_X - mag_X) * (map_X - mag_X) + (map_Y - mag_Y) * (map_Y - mag_Y) + (map_Z - mag_Z) * (map_Z - mag_Z);

                // 단위벡터를 위한 벡터의 길이구하기(지도상의 지자기)
                m_v_leng = Math.sqrt(map_X * map_X + map_Y * map_Y + map_Z * map_Z);
                m_unit[0] = map_X / m_v_leng;
                m_unit[1] = map_Y / m_v_leng;
                m_unit[2] = map_Z / m_v_leng;

                unitRMS_min = Math.acos(c_unit[0] * m_unit[0] + c_unit[1] * m_unit[1] + c_unit[2] * m_unit[2]);

                // 현재는 센서가 하나라서 norm 벡터값이 하나이다.
                normRMS_min = Math.abs((map_X * map_X + map_Y * map_Y + map_Z * map_Z)- (mag_X * mag_X + mag_Y * mag_Y + mag_Z * mag_Z));

                check++;
            } else {
                curRMS = (map_X - mag_X) * (map_X - mag_X) + (map_Y - mag_Y) * (map_Y - mag_Y) + (map_Z - mag_Z) * (map_Z - mag_Z);

                // 단위벡터를 위한 벡터의 길이구하기(지도상의 지자기)
                m_v_leng = Math.sqrt(map_X * map_X + map_Y * map_Y + map_Z * map_Z);
                m_unit[0] = map_X / m_v_leng;
                m_unit[1] = map_Y / m_v_leng;
                m_unit[2] = map_Z / m_v_leng;
              //  Log.i("Success filtering", "m_unit[0] : " + m_unit[0] + ", m_unit[1] : " + m_unit[1] + ", m_unit[2] : " + m_unit[2] + ", x : " + (c_unit[0] * m_unit[0] + c_unit[1] * m_unit[1] + c_unit[2] * m_unit[2]) );

                unitRMS_cur = Math.acos(c_unit[0] * m_unit[0] + c_unit[1] * m_unit[1] + c_unit[2] * m_unit[2]);
                normRMS_cur = Math.abs((map_X * map_X + map_Y * map_Y + map_Z * map_Z)- (mag_X * mag_X + mag_Y * mag_Y + mag_Z * mag_Z));

               // Log.i("Success filtering", "normRMS_cur : " + normRMS_cur + ", unitRMS_cur : " + unitRMS_cur);
                //norm벡터
                if (normRMS_cur < normRMS_min) {
                    norm_pos[0] = pos_X;
                    norm_pos[1] = pos_Y;
                    norm_pos[2] = pos_Theta;
                }

                //unit벡터
                if (Math.abs(unitRMS_cur) < (Math.abs(unitRMS_min))) {
                    unit_pos[0] = pos_X;
                    unit_pos[1] = pos_Y;
                    unit_pos[2] = pos_Theta;
                }

                //현재의 RMS 값이 더 작다면 갱신
                if (curRMS < minRMS) {
                    raw_pos[0] = pos_X;          // pos_X
                    raw_pos[1] = pos_Y;          // pos_Y
                    raw_pos[2] = pos_Theta;      // theta

                    minRMS = curRMS;

                    //Log.i("******Magnetic sensor", "Map_X : " + map_X + ", Map_Y : " + map_Y + ", Map_Z : " + map_Z + ", MinRMS : " + minRMS);
                    //Log.i("Magnetic CurRMS", "CurRMS : " + curRMS);
                }
            }
            //Log.i("Magnetic sensor", "test : " + test + ", check : "+ check);
            //test++;
        }

        /*if (Math.abs((raw_pos[0] - unit_pos[0])) + Math.abs((raw_pos[1] - unit_pos[1])) <= 4 || Math.abs((raw_pos[0] - norm_pos[0])) + Math.abs((raw_pos[1] - norm_pos[1])) <= 4) {
            Log.i("jkk success filtering", "raw_pos[0] : " + raw_pos[0] + ", raw_pos[1] : " + raw_pos[1] + ", raw_pos[2] : " + raw_pos[2]);
            return raw_pos;
        }

        //Log.i("******Magnetic sensor", "Map_X : " + map_X + ", Map_Y : " + map_Y + ", Map_Z : " + map_Z + ", MinRMS : " + minRMS);
        //Log.i("Magnetic FinalMinRMS", "MinRMS : " + minRMS);
        raw_pos[2] = -1;
        Log.i("jkk fail raw filtering", "raw_pos[0] : " + raw_pos[0] + ", raw_pos[1] : " + raw_pos[1] + ", raw_pos[2] : " + raw_pos[2]);
        Log.i("jkk fail unit filtering", "unit_pos[0] : " + unit_pos[0] + ", unit_pos[1] : " + unit_pos[1] + ", unit_pos[2] : " + unit_pos[2]);
        Log.i("jkk fail norm filtering", "norm_pos[0] : " + norm_pos[0] + ", norm_pos[1] : " + norm_pos[1] + ", norm_pos[2] : " + norm_pos[2]);

        //Log.i("jkk fail filtering", "raw_pos[0] : " + raw_pos[0] + ", raw_pos[1] : " + raw_pos[1] + ", raw_pos[2] : " + raw_pos[2]);
        */
        return raw_pos;

    }
}
