package com.example.jeyun.indoormap;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by gksdu on 2018-01-12.
 */

// 외부  DB를 복사하여 내부 DB로 저장하는 Class
public class MySQLiteOpenHelper extends ContentProvider {

    Context context;
    String name;
    SQLiteDatabase mDB;
    DBHelper helper;

    public MySQLiteOpenHelper(Context context, String name)
    {
        this.context = context;

        this.name = name;
        copyDB();

    }

    @Override
    public boolean onCreate()
    {
        // 내부 DB로 복사해서 저장
        return true;
    }

    //Log.i("","");
    private void copyDB() {
        try {
            boolean bResult = checkDB(context);
            if(!bResult)
            {
                doCopy(context, this.name);
            }
            else
            {
                deletefile(context);
                doCopy(context, this.name);
            }
        }catch (Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsStrting = sw.toString();
            Log.e("copyDB error",exceptionAsStrting);
        }
    }

    // 내부 DB 옮겨오기전에 이미 있는 파일인지 체크
    private boolean checkDB(Context mContext) {
        String filePath = "/data/data/" + mContext.getPackageName() + "/databases/" + this.name;
        File file = new File(filePath);
        if( file.exists())
        {
            //있으면  true 반환
            return true;
        }
        return false;
    }

    private void deletefile(Context mContext)
    {
        String folderPath = "/data/data/" + mContext.getPackageName() + "/databases";
        String filePath = folderPath + "/" + this.name;
        File folder = new File(folderPath);
        File file = new File(filePath);

        file.delete();
    }


    private void doCopy(Context mContext, String name3)
    {
        AssetManager manager = mContext.getAssets();
        String folderPath = "/data/data/" + mContext.getPackageName() + "/databases";
        String filePath = folderPath + "/" + name3;
        File folder = new File(folderPath);
        File file = new File(filePath);
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open(name3);
            BufferedInputStream bis = new BufferedInputStream(is);
            if (folder.exists()) {
            } else {
                folder.mkdirs();
            }
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }
            bos.flush();
            bos.close();
            fos.close();
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e("ErrorMessage : ", e.getMessage());
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String sql = null;

        Cursor cursor = mDB.rawQuery(sql, selectionArgs);
        return cursor;
    }

    @Override
    public String getType(Uri uri)
    {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}

